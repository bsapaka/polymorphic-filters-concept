@extends('app')

@section('content')
    {!! Form::model($filterAttributes,['method' => 'GET','action' => ['ItemsController@index']]) !!}
        <h3>Item</h3>
        <ul>
            <li class="form-group">
                {!! Form::label('item_name', 'Name') !!}
                {!! Form::input('string', 'item_name') !!}
            </li>
            <li class="form-group">
                <p><strong>Weight</strong></p>
                {!! Form::label('item_weight_min', 'Min:') !!}
                {!! Form::input('number', 'item_weight_min') !!}

                {!! Form::label('item_weight_max', 'Max:') !!}
                {!! Form::input('number', 'item_weight_max') !!}
            </li>
        </ul>
        <h3>Equipment</h3>
            <ul>
                <li class="form-group">
                    <p><strong>Condition</strong></p>
                    {!! Form::label('eq_condition_min', 'Min:') !!}
                    {!! Form::input('number', 'eq_condition_min') !!}

                    {!! Form::label('eq_condition_max', 'Max:') !!}
                    {!! Form::input('number', 'eq_condition_max') !!}
                </li>
            </ul>
        <h3>User</h3>
            <ul>
                <p><strong>Join Date</strong></p>
                {!! Form::label('user_created_at_min', 'Min:') !!}
                {!! Form::input('date', 'user_created_at_min') !!}

                {!! Form::label('user_created_at_max', 'Max:') !!}
                {!! Form::input('date', 'user_created_at_max') !!}
            </ul>

        <div class="form-group">
            {!! Form::Submit(
                    'Filter',
                    ['class' => 'btn btn-success']
            ) !!}
        </div>
    {!! Form::close() !!}

@stop