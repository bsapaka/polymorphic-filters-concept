<?php

namespace App\Http\Controllers;

//use Symfony\Component\HttpFoundation\Request;
use App\Equipment;
use App\Item;
use App\PMF\EquipmentFilter;
use App\PMF\ItemFilter;
use App\PMF\PolymorphicFilter;
use App\PMF\UserFilter;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class ItemsController extends Controller
{

    /**
     * @var ItemFilter
     */
    protected $pmf;

    public function __construct(PolymorphicFilter $pmf)
    {
        $this->pmf = $pmf;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $requestAttributes = array_filter($request->all(), function($value) {
            return $value !== "";
        });
        
        $this->pmf
            ->setModelInstance(new Item())
            ->setSubjectFilter(new ItemFilter())
            ->hasMany(new EquipmentFilter())
            ->belongsTo(new UserFilter())
            ->filterOn($requestAttributes);
        
        $filterAttributes = [];
        return view('items.index')->with(['filterAttributes' => $filterAttributes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
