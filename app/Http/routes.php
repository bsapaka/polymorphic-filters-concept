<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('app');
});

Route::resource('items', 'ItemsController');

Route::get('foo', function() {

//    $q = \App\User::with('items')->get();//
//
//    $q = \App\User::with('items')->getQuery()->toSql();
//
//    $q = \App\User::with('items')->where('weight','>',4)->getQuery()->toSql();
//
//    $i = new \App\Item();
//    $q = \App\Item::with('user')->where('weight','>',2)->get();

//    $q = \App\User::with(['items' => function($query) {
//        $query->where('weight','>',1);
//    }])->get();

    $items = \App\Item::where('weight', '>', '3')->get();
    $users = [];
    foreach ($items as $item) {
        $users[$item->user->id] = $item->user;
    }

    dd($users);
});

