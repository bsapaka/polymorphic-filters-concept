<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model {
	
	protected $table = 'items';

	public function user() {
		return $this->belongsTo('App\User');
	}

	public function equipment() {
		return $this->hasMany('App\Equipment');
	}

}