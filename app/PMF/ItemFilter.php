<?php namespace App\PMF;

use App\Equipment;
use App\Item;
use App\User;

class ItemFilter extends ModelFilter {

	protected $modelClass = Item::class;
	
	protected $fieldPrefix = 'item';
	
	protected $hasMany = [
		Equipment::class => 'equipment'	
	];
	
	protected $belongsTo = [
		User::class => 'user'	
	];

}