<?php namespace App\PMF;

use App\Equipment;
use App\Item;
use App\User;

class EquipmentFilter extends ModelFilter {

	protected $modelClass = Equipment::class;

	protected $fieldPrefix = 'eq';

	protected $belongsTo = [
		Item::class => 'item',
		User::class => 'user'
	];
}