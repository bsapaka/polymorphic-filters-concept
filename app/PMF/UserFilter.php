<?php namespace App\PMF;

use App\Equipment;
use App\Item;
use App\User;

class UserFilter extends ModelFilter {

	protected $modelClass = User::class;
	
	protected $fieldPrefix = 'user';
	
	protected $hasMany = [
		Item::class => 'items',
		Equipment::class => 'equipment'
	];
}