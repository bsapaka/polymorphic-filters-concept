<?php namespace App\PMF\FieldFilters;


use Illuminate\Database\Eloquent\Model;

class MaxFilter extends FieldFilter {

	protected $postfix = 'fmax';

	protected function filter(Model $model) {
		return $model->where($this->inputField, '<=', $this->inputValue);
	}
}