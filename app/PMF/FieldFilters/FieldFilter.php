<?php namespace App\PMF\FieldFilters;

use Illuminate\Database\Eloquent\Model;

abstract class FieldFilter {

	/**
	 * @var String
	 *
	 * The input field name
	 */
	protected $inputField;

	/**
	 * @var mixed
	 *
	 * The input field value
	 */
	protected $inputValue;

	/**
	 * @var string
	 * 
	 * The postfix of the input field name that identifies what 
	 * type of filter to apply to the field
	 */
	protected $postfix;

	/**
	 * @param Model $model
	 * @return array
	 */
	public function pivotIds(Model $model) {
		$pivotIds = [];
		$instances = $this->filter($model)->all();
		/** @var Model $instance */
		foreach ($instances as $instance) {
			$pivotIds[] = $instance->getKey();
		}
		return $pivotIds;
	}

	/**
	 * @param Model $model
	 * @return Model
	 */
	protected function filter(Model $model) {
		return $model;
	}

	/**
	 * @return String
	 */
	public function getInputField() {
		return $this->inputField;
	}

	/**
	 * @param String $inputField
	 * @return FieldFilter
	 */
	public function setInputField($inputField) {
		$this->inputField = $this->removePostfix($inputField);

		return $this;
	}


	/**
	 * @param $inputField
	 * @return string
	 */
	protected function removePostfix($inputField) {
		// TODO: remove the postFix	
	}

	/**
	 * @return mixed
	 */
	public function getInputValue() {
		return $this->inputValue;
	}

	/**
	 * @param mixed $inputValue
	 * @return FieldFilter
	 */
	public function setInputValue($inputValue) {
		$this->inputValue = $inputValue;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getPostfix() {
		return $this->postfix;
	}
	
	

}