<?php namespace App\PMF\FieldFilters;


use App\PMF\ModelFilter;

class FieldFilterResolver {

	protected $filterClasses = [
		MinFilter::class,
		MaxFilter::class,
		MatchFilter::class
	];

	protected $filterInstances = [
		// postfix => instance
	];

	public function __construct() {
		$this->instantiateFilters();
	}


	/**
	 * @param $postfix
	 * @return ModelFilter
	 */
	public function getFilter($postfix) {
		if(in_array($postfix,$this->filterInstances)) {
			return $this->filterInstances[$postfix];
		}
	}

	/**
	 * @param ModelFilter $filter
	 * @return $this
	 */
	public function addFilterInstance(ModelFilter $filter) {
		$this->filterInstances = $filter;
		return $this;
	}

	/**
	 * Make an instance of each filter class
	 */
	protected function instantiateFilters() {
		foreach ($this->filterClasses as $class) {
			/** @var FieldFilter $filter */
			$filter = \App::make($class);
			$this->filterInstances[$filter->getPostfix()] = $filter;
		}
	}

}