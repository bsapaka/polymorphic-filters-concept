<?php namespace App\PMF\FieldFilters;


class MinFilter extends FieldFilter {

	protected $postfix = 'fmin';

	protected function filter(Model $model) {
		return $model->where($this->inputField, 'like', "%{$this->inputValue}%");
	}

}