<?php namespace App\PMF;

use Illuminate\Database\Eloquent\Model;
use PhpParser\Node\Expr\BinaryOp\Mod;

class PolymorphicFilter {

	/**
	 * @var Model
	 */
	protected $modelInstance;

	/**
	 * @var ModelFilter;
	 *
	 */
	protected $subjectFilter;

	/**
	 * @var array
	 *
	 * The subject model has many of the models of these filters
	 */
	protected $hasManyFilters;

	/**
	 * @var array
	 *
	 * The subject model belongs to the models of these filters
	 */
	protected $belongsToFilters;

	/**
	 * @var PolymorphicFilter
	 *
	 * Nested PMF
	 */
	protected $polymorphicFilter;

	public function filterOn(array $requestInput) {
		$model = $this->modelInstance;

		/** @var ModelFilter $filter */
		foreach ($this->belongsToFilters as $filter) {
			$filter->appendWhereClausesTo($model, $requestInput);
		}
		
		/** @var ModelFilter $filter */
		foreach ($this->hasManyFilters as $filter) {
			$filter->appendWhereAggregateClasesTo($model, $requestInput);
		}
		
	}

	/**
	 * @param Model $modelInstance
	 * @return $this
	 */
	public function setModelInstance(Model $modelInstance) {
		$this->modelInstance = $modelInstance;
		return $this;
	}

	/**
	 * @param ModelFilter $subjectFilter
	 * @return $this
	 */
	public function setSubjectFilter(ModelFilter $subjectFilter) {
		$this->subjectFilter = $subjectFilter;
		return $this;
	}

	/**
	 * @param ModelFilter $modelFilter
	 * @return $this
	 */
	public function hasMany(ModelFilter $modelFilter) {
		$this->hasManyFilters[$modelFilter->getFieldPrefix()] = $modelFilter;
		return $this;
	}

	/**
	 * @param ModelFilter $modelFilter
	 * @return $this
	 */
	public function belongsTo(ModelFilter $modelFilter) {
		$this->belongsToFilters[$modelFilter->getFieldPrefix()] = $modelFilter;
		return $this;
	}

	/**
	 * @param $polymorphicFilter
	 * @return $this
	 */
	public function setPolymorphicFilter($polymorphicFilter) {
		$this->polymorphicFilter = $polymorphicFilter;
		return $this;
	}


}