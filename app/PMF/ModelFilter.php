<?php namespace App\PMF;


use App\PMF\FieldFilters\FieldFilter;
use App\PMF\FieldFilters\FieldFilterResolver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

abstract class ModelFilter {

	/**
	 * @var string
	 *
	 * The full name of this filter's model
	 */
	protected $model;

	/**
	 * @var string
	 *
	 * The input field prefix that indicates to use this filter
	 */
	protected $fieldPrefix;

	/**
	 * @var FieldFilterResolver
	 */
	protected $fieldFilterResolver;


	public function __construct(FieldFilterResolver $fieldFilterResolver) {
		$this->fieldFilterResolver = $fieldFilterResolver;
	}

	/**
	 * @param Model $subjectModel
	 * @param array $requestInput
	 */
	public function appendWhereClausesTo(Model $subjectModel, array $requestInput) {
		foreach ($requestInput as $rawField => $value) {
			$fieldPrefix = substr($rawField, 0, strpos($rawField,'_'));
			if($fieldPrefix === $this->fieldPrefix) {
				/** @var Model $pivotModel */
				$pivotModel = \App::make($this->model);
				/** @var FieldFilter $filter */
				$fieldPostfix = substr($rawField, strrpos($rawField, '_'));
				$filter = $this->fieldFilterResolver->getFilter($fieldPostfix);

				$pivotIds = $filter->setInputValue($value)
					->setInputField($this->removePrefix($rawField))
					->pivotIds($pivotModel);

				$subjectModel->whereIn($pivotModel->getKeyName(), $pivotIds);

			}
		}
	}



	/**
	 * @param String $rawField
	 * @return String
	 *
	 * Remove the prefix and postfix from the raw field
	 */
	protected function removePrefix($rawField) {
		//TODO: remove prefix
	}

	public function appendWhereClauseTo(Model $model) {
		$inputField = $this->inputField;
		$inputValue = $this->inputValue;

		if ($this->inputFieldIsMin()) {
			$field = str_replace($this->minPostfix, "", $inputField);
			$model = $model->where($field, '>=', $inputValue);
		} elseif ($this->inputFieldIsMax()) {
			$field = str_replace($this->maxPostfix, "", $inputField);
			$model = $model->where($field, '<=', $inputValue);
		} else {
			$field = $inputField;
			$model = $model->where($field, "like", "%{$inputValue}%");
		}

		return $model;
	}

	public function appendWhereAggregateClasesTo(Model $modelInstance, array $requestInput) {

	}

	/**
	 * @return string
	 */
	public function getFieldPrefix() {
		return $this->fieldPrefix;
	}
	
	

	

}