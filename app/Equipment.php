<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipment extends Model
{
	protected $table = 'equipment';

	public function item() {
		return $this->belongsTo('App\Item');
	}

	public function user() {
		return $this->belongsTo('App\User');
	}

}